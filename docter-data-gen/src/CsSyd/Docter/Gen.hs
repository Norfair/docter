{-# OPTIONS_GHC -fno-warn-orphans #-}

module CsSyd.Docter.Gen where

import Import

import CsSyd.Docter

instance GenUnchecked OutputDirective

instance GenValid OutputDirective where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid PhoneNumber where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid Address where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid EmailAddress where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid PaymentDetails where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid IBAN where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid Price where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
