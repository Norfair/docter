{-# LANGUAGE TypeApplications #-}

module CsSyd.Docter.GenSpec
  ( spec
  ) where

import TestImport

import CsSyd.Docter

import CsSyd.Docter.Gen ()

spec :: Spec
spec = do
  genValidSpec @OutputDirective
  genValidSpec @PhoneNumber
  jsonSpecOnValid @PhoneNumber
  genValidSpec @Address
  jsonSpecOnValid @Address
  genValidSpec @EmailAddress
  jsonSpecOnValid @EmailAddress
  genValidSpec @PaymentDetails
  jsonSpecOnValid @PaymentDetails
  genValidSpec @IBAN
  jsonSpecOnValid @IBAN
  genValidSpec @Price
  jsonSpecOnValid @Price
