{-# LANGUAGE FlexibleContexts #-}

module CsSyd.Docter
  ( genPdfFor
  , hidden
  , writeJSON
  , writeYaml
  , readJSONOrYaml
  , searchFile
  , module CsSyd.Docter.Types
  ) where

import Control.Monad
import Data.Aeson as JSON
import Data.Aeson.Encode.Pretty as JSON
import qualified Data.ByteString as SB
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Char8 as LB8
import Data.List
import Data.Maybe
import qualified Data.Text.IO as T
import Data.Yaml as Yaml
import Path
import Path.IO
import System.Exit
import System.Process
import Text.Mustache

import CsSyd.Docter.Types

genPdfFor :: ToJSON (ForMustache a) => Template -> Path Abs File -> a -> IO ()
genPdfFor tmpl file val = do
  let mustacheVal = ForMustache val
  writeJSON file mustacheVal
  texFile <- setFileExtension ".tex" file
  let (errs, texContents) = checkedSubstitute tmpl $ toJSON mustacheVal
  unless (null errs) $
    die $
    unlines $
    [ "Substitution errors in template when substituting with data"
    , LB8.unpack $ encodePretty mustacheVal
    ] ++
    map show errs
  T.writeFile (toFilePath texFile) texContents
  let cmd = unwords ["latexmk", "-pdf", toFilePath $ filename texFile]
  (_, _, _, ph) <-
    createProcess $ (shell cmd) {cwd = Just $ toFilePath $ parent file}
  ec <- waitForProcess ph
  case ec of
    ExitSuccess -> pure ()
    ExitFailure c ->
      die $ unwords ["Command", cmd, "failed with exit code", show c]

-- TODO filter all hidden files!
hidden :: Path Abs File -> Bool
hidden f = ".swp" `isSuffixOf` fileExtension f

writeJSON :: ToJSON a => Path Abs File -> a -> IO ()
writeJSON f a = do
  ensureDir $ parent f
  LB.writeFile (toFilePath f) (JSON.encodePretty a)

writeYaml :: ToJSON a => Path Abs File -> a -> IO ()
writeYaml f a = do
  ensureDir $ parent f
  SB.writeFile (toFilePath f) (Yaml.encode a)

readJSONOrYaml :: FromJSON a => Path Abs File -> IO a
readJSONOrYaml f = do
  mc <- forgivingAbsence $ LB.readFile $ toFilePath f
  case mc of
    Nothing -> die $ unwords ["File", toFilePath f, "not found."]
    Just contents ->
      case JSON.eitherDecode contents of
        Right res -> pure res
        Left err ->
          case Yaml.decodeEither' (LB.toStrict contents) of
            Left yerr ->
              die $
              unlines
                [ "Failed to decode file as either json or yaml"
                , toFilePath f
                , "json decoding error:"
                , show err
                , "yaml decoding error:"
                , show yerr
                ]
            Right res -> pure res

searchFile :: Path Abs Dir -> Path Rel File -> IO (Maybe (Path Abs File))
searchFile root file = do
  files <- snd <$> listDirRecur root
  pure $
    find
      (\f ->
         isJust $
         stripPrefix (reverse $ toFilePath file) (reverse $ toFilePath f))
      files
